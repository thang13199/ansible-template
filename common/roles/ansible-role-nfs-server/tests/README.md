# Tests

- Basic tests for the playbooks and tasks come here. Generally you need a `test.yml` and `inventory` file with at least `localhost`
- You can write some test like ping-pong with redis, connect and query database with sql

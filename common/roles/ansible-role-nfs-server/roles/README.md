# Roles

If you are wanting to use role specifically for this, you will need to define it in a `requirements.yml`, otherwise AWX will not install what you need to run your tasks.

Example:

```
---
# Roles
roles:
  - rockylinux.ipagetcert
    src: https://github.com/rocky-linux/ansible-role-ipa-getcert
    version: main
```

You should define your sub-roles clearly

Example:

```
---

├── roles
│   └── common
│   └── redis-sentinel
│   └── redis-cluster
```

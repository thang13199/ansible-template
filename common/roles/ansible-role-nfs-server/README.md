# Monitoring

This repository is for all ansible templates.

```
.
├── README.md
├── defaults
│   └── main.yml
├── files
│   └── README.md
├── handlers
│   └── main.yml
├── tasks
│   └── main.yml
├── templates
│   └── README.md
├── tests
│   ├── README.md
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```

## Guidelines

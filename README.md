# Provides / Information

This repository is for ansible templates.

```
.
├── README.md
├── logging
├── monitoring
├── queue-streaming-caching
```

## Guidelines
This project using yamllint and ansible-lint to validate code quality
- Installation:
```
pip install yamllint
pip3 install ansible-lint
pip3 install pre-commit
```
- Usage:
```
pre-commit run --all-files
```
- Yamllint and ansible-lint auto run when commit with pre-commit-hook
